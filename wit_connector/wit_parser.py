from conf.feature import wit_config
import wit
import simplejson as json

class Wit_parser:
    def parse_input(self, query):
        wit.init()
        response = wit.text_query(query, wit_config["wit_auth_key"])
        wit.close()
        response = json.loads(response)
        return response["outcomes"][0]