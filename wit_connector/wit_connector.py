from chat_ui import communicator
from core.strategies import strategies
from strategist.strategist import Strategist
from utils.say import say
from wit_parser import Wit_parser
from conf.feature import wit_config

times = {
    "morning": xrange(0, 12),
    "afternoon": xrange(12, 16),
    "evening": xrange(16, 20),
    "night": xrange(20, 24)
}


class Wit_connector:
    def __init__(self, sentence):
        communicator.send_to_ui("user", sentence)
        sentence = self.command_exists(sentence)
        self.sentence = sentence

    def command_exists(self, sentence):
        command = "ok jeeves"
        if command in sentence:
            sentence = sentence.replace(command, "")
        elif 1 == 1:
            say("please include the command.")
            sentence = ""
        return sentence

    def check_if_greeting(self):
        greetings = ['morning', 'afternoon', 'evening', 'night']
        casual_greetings = ['hello', 'hi', 'hey']

        for greeting in greetings:
            if greeting in self.sentence:
                self.react(greeting)
                return True
        for casual_greeting in casual_greetings:
            if casual_greeting in self.sentence:
                self.react_casually(casual_greeting)
                return True

        return False

    def react(self, greeting):
        time_of_day = datetime.now().hour

        if time_of_day not in times[greeting]:
            say("Actually, its...")

        for key in times.keys():
            if time_of_day in times[key]:
                say("Good %s" % key)
                break

    def segregate_and_react(self):
        if self.sentence == "":
            say("please say something")
        elif not self.check_if_greeting():
            parser = Wit_parser()
            parsedInput = parser.parse_input(self.sentence)
            intent = parsedInput["intent"]
            entities = parsedInput["entities"]
            strategist = Strategist(strategies)
            strategist.get_strategy_for(intent, entities)

    def react_casually(self, casual_greeting):
        say(casual_greeting)
